describe("gameMovement.isInputValid", function() {

  it("Single orientation should be valid", function() {
    assertOrdinal('N', true);
    assertOrdinal('S', true);
    assertOrdinal('E', true);
    assertOrdinal('W', true);
    assertOrdinal('U', true);
    assertOrdinal('D', true);
  });

  it("Opposing ordinals together should be invalid", function() {
    assertOrdinal('NS', false);
    assertOrdinal('EW', false);
    assertOrdinal('UD', false);
  });

  it("Two ordinal combinations should be valid.", function() {
    assertOrdinal('NW', true);
    assertOrdinal('SW', true);
    assertOrdinal('SE', true);
    assertOrdinal('NE', true);
    assertOrdinal('NU', true);
    assertOrdinal('SU', true);
    assertOrdinal('SD', true);
    assertOrdinal('ND', true);
    assertOrdinal('ED', true);
    assertOrdinal('EU', true);
    assertOrdinal('WU', true);
    assertOrdinal('WD', true);
  });
  
  it("Three ordinal combinations should be valid.", function() {
    assertOrdinal('NWU', true);
    assertOrdinal('NWD', true);
    assertOrdinal('SWU', true);
    assertOrdinal('SWD', true);
    assertOrdinal('SEU', true);
    assertOrdinal('SED', true);
    assertOrdinal('NEU', true);
    assertOrdinal('NED', true);
  });

  it("Opposing ordinals together should be invalid even if part of three", function() {
    assertOrdinal('NSU', false);
    assertOrdinal('EWN', false);
    assertOrdinal('UDW', false);
  });

  it("Should reject any string containing chars other than u d n e s w.", function() {
    assertOrdinal('A', false);
    assertOrdinal('EB', false);
    assertOrdinal('UNZ', false);
  });

  it("Should accept valid strings in any case.", function() {
    assertOrdinal('nWu', true);
    assertOrdinal('sd', true);
    assertOrdinal('SD', true);
  });
  
});

describe("gameMovement.getRotationQuaternionFromGameDirection", function() {

  it("N should rotate around y axis", function() {
    var result = DOGFIGHT.getRotationQuaternionFromGameDirection('N');
    var expectedQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(-Math.PI/2,0,0);
    expect(result).toEqual(expectedQuaternion);
  });
  
  it("S should rotate around y axis", function() {
    var result = DOGFIGHT.getRotationQuaternionFromGameDirection('S');
    var expectedQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(Math.PI/2,0,0);
    expect(result).toEqual(expectedQuaternion);
  });
  
  it("E should not rotate", function() {
    var result = DOGFIGHT.getRotationQuaternionFromGameDirection('E');
    var expectedQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(0,0,0);
    expect(result).toEqual(expectedQuaternion);
  });
  
  it("W should rotate around y axis", function() {
    var result = DOGFIGHT.getRotationQuaternionFromGameDirection('W');
    var expectedQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(Math.PI,0,0);
    expect(result).toEqual(expectedQuaternion);
  });

  it("U should rotate around x axis", function() {
    var result = DOGFIGHT.getRotationQuaternionFromGameDirection('U');
    var expectedQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(0,-Math.PI/2,0);
    expect(result).toEqual(expectedQuaternion);
  });

  it("D should rotate around x axis", function() {
    var result = DOGFIGHT.getRotationQuaternionFromGameDirection('D');
    var expectedQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(0,Math.PI/2,0);
    expect(result).toEqual(expectedQuaternion);
  });
});

describe("gameMovement.getPitch", function() {
  it("no up or down should be zero", function(){
      assertPitch('NW', 0);
      assertPitch('E', 0);
  });
  
  it("just up or down should be +- 90 degrees", function(){
      assertPitch('U', -Math.PI/2);
      assertPitch('D', Math.PI/2);
  });

  it("up or down and a direction should be 45 degrees.", function(){
      assertPitch('UE', Math.PI/4);
      assertPitch('DSW', -Math.PI/4);
  });
});
  
describe("gameMovement.getYawDirty", function() {
  it("n should be -r90", function(){
      assertGetYaw('N', -Math.PI/2);
      assertGetYaw('s', Math.PI/2);
  });

  it("e should be 0, w r180", function(){
      assertGetYaw('e', 0);
      assertGetYaw('W', Math.PI);
  });

  it("ne should be -45", function(){
      assertGetYaw('ne', -Math.PI/4);
      assertGetYaw('se', Math.PI/4);
  });

  it("nw should be -135", function(){
      assertGetYaw('nw', -3*Math.PI/4);
      assertGetYaw('sw', 3*Math.PI/4);
  });

  it("should ignore u and d", function(){
    assertGetYaw('nwu', -3*Math.PI/4);
    assertGetYaw('sd', Math.PI/2);
    assertGetYaw('u', undefined);
    assertGetYaw('D', undefined);
  });
});

function assertPitch(gameDirection, expectedPitch){
      var pitch = DOGFIGHT.getPitch(gameDirection);
      expect(pitch).toEqual(expectedPitch);
};

function assertQuaternion(expectedYaw, expectedPitch, expectedRoll, actualQuart){
    var expectedQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(expectedYaw,expectedPitch,expectedRoll);
    expect(actualQuart).toEqual(expectedQuaternion);
};

function assertOrdinal(ordinal, expectedValidity){
    var result = DOGFIGHT.isInputValid(ordinal);
    expect(result).toEqual(expectedValidity);
};

function assertGetYaw(gameDirection, expectedYaw){
      var yaw = DOGFIGHT.getYawDirty(gameDirection);
      expect(yaw).toEqual(expectedYaw);
};