var DOGFIGHT = DOGFIGHT ? DOGFIGHT : {};
DOGFIGHT.scene = {};
window.onload = function(){
    var canvas = document.getElementById("canvas");

    // Check support
    if (!BABYLON.Engine.isSupported()) {
        window.alert('Browser not supported');
    } else {
        // Babylon
        var engine = new BABYLON.Engine(canvas, true);

        //Creating scene (in "scene_tuto.js")
        DOGFIGHT.scene = createScene(engine);

        DOGFIGHT.scene.activeCamera.attachControl(canvas);


        // Once the scene is loaded, just register a render loop to render it
        engine.runRenderLoop(function () {
            DOGFIGHT.scene.render();
        });

        // Resize
        window.addEventListener("resize", function () {
            engine.resize();
        });

    } 
};

window.addEventListener("keydown", function(evt){
    if (evt.keyCode == 65) //A
    {
        //test chaining together animations
        simpleMovePlane(
            DOGFIGHT.scene, 
            DOGFIGHT.redPlane2, 
            BABYLON.Quaternion.RotationYawPitchRoll(Math.PI/4,Math.PI/4,Math.PI/2),
            new BABYLON.Vector3(10, 20, 50),
            function(){
                    simpleMovePlane(DOGFIGHT.scene, 
                    DOGFIGHT.redPlane1, 
                    BABYLON.Quaternion.RotationYawPitchRoll(Math.PI/4,Math.PI/4,Math.PI/2),
                    DOGFIGHT.redPlane1.position.add(new BABYLON.Vector3(10, 20, 50))
                );
            }
        );
        simpleMovePlane(DOGFIGHT.scene, 
            DOGFIGHT.redPlane2, 
            
            new BABYLON.Vector3(10, 20, 10)
        );
    };
    if (evt.keyCode == 67) //C
    {
        //test getting babylon rotations from game rotations.
        simpleMovePlane(DOGFIGHT.scene, 
            DOGFIGHT.bluePlane1, 
            DOGFIGHT.getRotationQuaternionFromGameDirection('unw'),
            DOGFIGHT.bluePlane1.position,
            function(){
                simpleMovePlane(DOGFIGHT.scene, 
                    DOGFIGHT.bluePlane1, 
                    DOGFIGHT.getRotationQuaternionFromGameDirection('u'),
                    DOGFIGHT.bluePlane1.position
                    ,
                    function(){
                        simpleMovePlane(DOGFIGHT.scene, 
                            DOGFIGHT.bluePlane1, 
                            DOGFIGHT.getRotationQuaternionFromGameDirection('n'),
                            DOGFIGHT.bluePlane1.position
                            ,
                            function(){
                                simpleMovePlane(DOGFIGHT.scene, 
                                DOGFIGHT.bluePlane1, 
                                DOGFIGHT.getRotationQuaternionFromGameDirection('se'),
                                DOGFIGHT.bluePlane1.position
                                );
                            }
                        );
                    }
                );
            } 
        );
    };
    if (evt.keyCode == 68) //D
    {
        // alert('d');
        var quartList = [];
        quartList.push(DOGFIGHT.getRotationQuaternionFromGameDirection('unw'));
        quartList.push(DOGFIGHT.getRotationQuaternionFromGameDirection('u'));
        quartList.push(DOGFIGHT.getRotationQuaternionFromGameDirection('n'));
        quartList.push(DOGFIGHT.getRotationQuaternionFromGameDirection('se'));
        quartList.push(DOGFIGHT.getRotationQuaternionFromGameDirection('s'));
        quartList.push(DOGFIGHT.getRotationQuaternionFromGameDirection('sd'));
        quartList.push(DOGFIGHT.getRotationQuaternionFromGameDirection('sdw'));
        quartList.push(DOGFIGHT.getRotationQuaternionFromGameDirection('dw'));
        quartList.push(DOGFIGHT.getRotationQuaternionFromGameDirection('w'));
        //test getting babylon rotations from game rotations.
        simpleMovePlane2(DOGFIGHT.scene, 
            DOGFIGHT.bluePlane1, 
            quartList,
            DOGFIGHT.bluePlane1.position
        );
    };
});
