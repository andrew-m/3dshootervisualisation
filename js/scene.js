
function createScene(engine) {
    //Creation of the scene 
    var scene = new BABYLON.Scene(engine);
	var planeMesh;

    //Adding the light to the scene
    var light = new BABYLON.PointLight("Omni", new BABYLON.Vector3(70, 400, 70), scene);

    //Adding a Free Camera
    var camera = new BABYLON.FreeCamera("Camera", new BABYLON.Vector3(20,20,-70), scene);

	drawSkybox(scene);
	drawCornerBlobs(scene);
	drawGrids(scene);
	
	BABYLON.SceneLoader.ImportMesh("Cube", "assets/", "phantom.babylon", scene, function(meshes){
		var parentPlane = meshes[0];
		parentPlane.isVisible = false;
		parentPlane.scaling = new BABYLON.Vector3(0.2,0.2,0.2);
		parentPlane.rotationQuaternion = BABYLON.Quaternion.RotationYawPitchRoll(0,0,0);

		var jigIntoCentreOfSquare = new BABYLON.Vector3(5,5,2)
		
		bluePlane1 = parentPlane.clone(parentPlane.name);
		bluePlane1.material = new BABYLON.StandardMaterial("planeMaterial", scene);
		bluePlane1.material.diffuseColor = new BABYLON.Color3(0.2,0.2,1.0);
		bluePlane1.position = new BABYLON.Vector3(0,10,0).add(jigIntoCentreOfSquare);
		bluePlane1.isVisible = true;
		
		bluePlane2 = parentPlane.clone(parentPlane.name);
		bluePlane2.material = new BABYLON.StandardMaterial("planeMaterial", scene);
		bluePlane2.material.diffuseColor = new BABYLON.Color3(0.2,0.2,1.0);
		bluePlane2.position = new BABYLON.Vector3(10,10,0).add(jigIntoCentreOfSquare);
		bluePlane2.isVisible = true;
		
		redPlane1 = parentPlane.clone(parentPlane.name);
		redPlane1.material = new BABYLON.StandardMaterial("planeMaterial", scene);
		redPlane1.material.diffuseColor = new BABYLON.Color3(1.0,0.2,0.2);
		redPlane1.position = new BABYLON.Vector3(20,10,0).add(jigIntoCentreOfSquare);
		redPlane1.isVisible = true;
		
		redPlane2 = parentPlane.clone(parentPlane.name);
		redPlane2.material = new BABYLON.StandardMaterial("planeMaterial", scene);
		redPlane2.material.diffuseColor = new BABYLON.Color3(1.0,0.2,0.2);
		redPlane2.position = new BABYLON.Vector3(30,10,0).add(jigIntoCentreOfSquare);
		redPlane2.isVisible = true;

		DOGFIGHT.bluePlane1 = bluePlane1;
		DOGFIGHT.bluePlane2 = bluePlane2;
		DOGFIGHT.redPlane1 = redPlane1;
		DOGFIGHT.redPlane2 = redPlane2;
	});

	return scene;
};

