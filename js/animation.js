
function simpleMovePlane(scene, plane, directionQuaternion, location, callback){
	if(plane === undefined){alert('wtf?');}
	if(scene === undefined){alert('wtf? Scene!');}
	
	startQuart = plane.rotationQuaternion;
	endQuart = directionQuaternion;

	plane.animations.push(getDirectionAnimation(startQuart, endQuart));

	plane.animations.push(
		getMovementAnimation(
			plane.position, 
			location
		)
	);
    scene.beginAnimation(plane, 0, 30, false, 1, callback);
}

function simpleMovePlane2(scene, plane, directionArray, location){
	if(plane === undefined){alert('wtf?');}
	if(scene === undefined){alert('wtf? Scene!');}
	
	plane.animations.push(getChainAnimation(directionArray));

	plane.animations.push(
		getMovementAnimation(
			plane.position, 
			location
		)
	);
    scene.beginAnimation(plane, 0, 30, false, 1);
}

function getDirectionAnimation(startQuaternion, endQuaternion){
	var animateDirection = new BABYLON.Animation(
	    "simpleDirectionAnimatePlane",
	    "rotationQuaternion",
	    60,
	    BABYLON.Animation.ANIMATIONTYPE_QUATERNION,
	    BABYLON.Animation.ANIMATIONLOOPMODE_RELATIVE
    );

    var keys1 = [];
    keys1.push({
        frame: 0,
        value: startQuaternion
    },
    {
        frame: 120,
        value: endQuaternion
    });
    animateDirection.setKeys(keys1);
    return animateDirection;
}

function getChainAnimation(quaternionArray){
	var quartArrayLength = quaternionArray.length;
	var animateDirection = new BABYLON.Animation(
	    "simpleDirectionAnimatePlane",
	    "rotationQuaternion",
	    60 * quartArrayLength,
	    BABYLON.Animation.ANIMATIONTYPE_QUATERNION,
	    BABYLON.Animation.ANIMATIONLOOPMODE_RELATIVE
    );
	var framesPerQuart = 60;
    var keys1 = [];
    for(var i = 0; i < quartArrayLength;i++){
    	var quart = quaternionArray[i];
    	console.error('quart: ' + quart);
    	var currentFrame = framesPerQuart * i;
    	console.error('frame: ' + currentFrame + " Value: " + quart);
    	keys1.push({frame: currentFrame, value: quart});
    }
    // keys1.push({
    //     frame: 0,
    //     value: startQuaternion
    // },
    // {
    //     frame: 120,lp;x
    //     value: endQuaternion
    // });
    animateDirection.setKeys(keys1);
    return animateDirection;
}

function getMovementAnimation(startVector3, endVector3){
	var animatePlanePosition = new BABYLON.Animation(
	    "animatePlanePosition",
	    "position",
	    60,
	    BABYLON.Animation.ANIMATIONTYPE_VECTOR3,
	    BABYLON.Animation.ANIMATIONLOOPMODE_RELATIVE
    );

    var keys = [];
    keys.push({
        frame: 0,
        value: startVector3
    },{
        frame: 30,
        value: endVector3
    });
    animatePlanePosition.setKeys(keys);
    return animatePlanePosition;
}