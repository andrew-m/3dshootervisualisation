var DOGFIGHT = DOGFIGHT ? DOGFIGHT : {};

var r45 = Math.PI/4; //45 degrees in radians etc.
var r90 = Math.PI/2;
var r135 = 3*Math.PI/4;
var r180 = Math.PI;
DOGFIGHT.getVectorFromGameLocation = function(gamex, gamey, gamez){
	var babylonx = gamex * 10;
	var babylony = gamez * 10;
	var babylonz = gamey * 10;
	return new BABYLON.Vector3(babylonx, babylony, babylonz);
};

DOGFIGHT.getRotationQuaternionFromGameDirection = function (gameDirection){
	var input = gameDirection.toLowerCase();
    var yaw = DOGFIGHT.getYawDirty(input);
    if (yaw === undefined){
    	yaw = 0;
    }
    var pitch = DOGFIGHT.getPitch(input);
    return BABYLON.Quaternion.RotationYawPitchRoll(yaw, pitch, 0);
};

DOGFIGHT.getYawDirty = function (gameDirection){
	var input = gameDirection.toLowerCase();

	if (input.contains('n')){
		if (input.contains('e')){
			return -r45;
		}
		 if (input.contains('w')){
			return -r135;
		}
		return -r90;
	}

	if (input.contains('s')){
		if (input.contains('e')){
			return r45;
		}
		 if (input.contains('w')){
			return r135;
		}
		return r90;
	}

	if (input.contains('e')){
		return 0;
	}
	 if (input.contains('w')){
		return r180;
	}
};


DOGFIGHT.getYaw = function (cleanYawInput){
	switch(cleanYawInput){
			case 'e':
	    		return 0;
			case 'se':
	    		return r45;
			case 's':
	    		return r90;
			case 'sw':
	    		return r135;
			case 'w':
	    		return r180;
			case 'nw':
	    		return -r135;
			case 'n':
	    		return -r90;
			case 'ne':
	    		return -r45;   			    			    			    		
		};
};

DOGFIGHT.getPitch = function(gameDirection){
	var input = gameDirection.toLowerCase();

	if (!input.contains('u') && !input.contains('d')){
		return 0;
	}

	if (input === 'u'){
		return -r90;
	}
	 if (input === 'd'){
		return r90;
	}

	if(input.contains('u')){
		return r45;
	}
	return -r45;

};

DOGFIGHT.isInputValid = function (gameDirection){
	var input = gameDirection.toLowerCase();
	if (input.contains('u') && input.contains('d')){
	 	return false;
	}
	if (input.contains('n') && input.contains('s')){
		return false;
	}
	if (input.contains('e') && input.contains('w')){
		return false;
	}
	 var allowedLetters = /[^udnesw]+/i;
	 if (input.match(allowedLetters)){
	 	return false;
	 }
	return true;
}; 
