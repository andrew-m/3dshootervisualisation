
function drawCornerBlobs(scene){
	var blobMaterial = new BABYLON.StandardMaterial("blobMaterial", scene);
	blobMaterial.diffuseColor = new BABYLON.Color3(0.9,0.4,0.8); //Kinda pink
	blobMaterial.specularColor = new BABYLON.Color3(0.3,0.15,0.3); //less shiny
	blobMaterial.emissiveColor = new BABYLON.Color3(0.45,0.2,0.4); //pretend there's more ambient light.
	
	var sphere1 = BABYLON.Mesh.CreateBox("Sphere1", 5, scene);
	var sphere2 = BABYLON.Mesh.CreateSphere("Sphere2",10, 6, scene);
	var sphere3 = BABYLON.Mesh.CreateSphere("Sphere3",10, 6, scene);
	var sphere4 = BABYLON.Mesh.CreateSphere("Sphere4",10, 6, scene);
	var sphere5 = BABYLON.Mesh.CreateSphere("Sphere5",10, 6, scene);
	var sphere6 = BABYLON.Mesh.CreateSphere("Sphere6",10, 6, scene);
	var sphere7 = BABYLON.Mesh.CreateSphere("Sphere7",10, 6, scene);
	var sphere8 = BABYLON.Mesh.CreateSphere("Sphere8",10, 6, scene);
	sphere1.position = new BABYLON.Vector3(0,0,0);
	sphere1.material = blobMaterial;
	sphere2.position = new BABYLON.Vector3(140,0,0);
	sphere2.material = blobMaterial;
	sphere3.position = new BABYLON.Vector3(0,140,0);
	sphere3.material = blobMaterial;
	sphere4.position = new BABYLON.Vector3(140,140,0);
	sphere4.material = blobMaterial;
	sphere5.position = new BABYLON.Vector3(0,0,140);
	sphere5.material = blobMaterial;
	sphere6.position = new BABYLON.Vector3(140,0,140);
	sphere6.material = blobMaterial;
	sphere7.position = new BABYLON.Vector3(0,140,140);
	sphere7.material = blobMaterial;
	sphere8.position = new BABYLON.Vector3(140,140,140);
	sphere8.material = blobMaterial;
};

function drawSkybox(scene){
	var skybox = BABYLON.Mesh.CreateBox("skyBox", 600.0, scene);

	// The sky creation
	var skyboxMaterial = new BABYLON.StandardMaterial("skyBox", scene);
	skyboxMaterial.backFaceCulling = false;
	skyboxMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
	skyboxMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
	skyboxMaterial.reflectionTexture = new BABYLON.CubeTexture("assets/simplejpg/simple", scene);
	skyboxMaterial.reflectionTexture.coordinatesMode = BABYLON.Texture.SKYBOX_MODE;

	// box + sky = skybox !
	skybox.material = skyboxMaterial;
	skybox.position = new BABYLON.Vector3(70,299.9,70);
}

function drawGrids(scene){
	var transparentGridMaterial = new BABYLON.StandardMaterial("grid", scene);
	transparentGridMaterial.diffuseTexture = new BABYLON.Texture("assets/GridMedium.png", scene);
    transparentGridMaterial.diffuseTexture.hasAlpha = true;
	transparentGridMaterial.specularColor = new BABYLON.Color3(0.0,0.0,0.0); //much less shiny!
	
	var parentGrid = BABYLON.Mesh.CreateGround("transparentGrid", 140, 140, 14, scene);
	parentGrid.isVisible = false;
    parentGrid.material = transparentGridMaterial;

	//All grids display /into/ the box, so your view into the box is not obscured.

	//nearet grid to starting camera point. xy plane at z = 0. Facing away (so you can't see it at first)
	var gridx1 = parentGrid.clone(parentGrid.name);
	gridx1.rotation.x = Math.PI/2; //rotate forward about x axis through centre of mesh.
	gridx1.position = new BABYLON.Vector3(140/2, 140/2, 0);
	gridx1.isVisible = true;
	
	//furthest grid zy plane at z = 140
	var gridx2 = parentGrid.clone(parentGrid.name);
	gridx2.rotation.x = -Math.PI/2; 
	gridx2.position = new BABYLON.Vector3(140/2, 140/2, 140);
	gridx2.isVisible = true;
	
	//right
	var gridz1 = parentGrid.clone(parentGrid.name);
	gridz1.rotation.z = Math.PI/2; 
	gridz1.position = new BABYLON.Vector3(140, 140/2, 140/2);
	gridz1.isVisible = true;
	
	//left
	var gridz2 = parentGrid.clone(parentGrid.name);
	gridz2.rotation.z = -Math.PI/2; 
	gridz2.position = new BABYLON.Vector3(0, 140/2, 140/2);
	gridz2.isVisible = true;
	
	//top
	var gridy1 = parentGrid.clone(parentGrid.name);
	gridy1.rotation.z = Math.PI; //flip it
	gridy1.position = new BABYLON.Vector3(140/2, 140, 140/2);
	gridy1.isVisible = true;
};
