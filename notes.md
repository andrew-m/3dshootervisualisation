Javascript queues,

var queue = [];
queue.push(2); //queue is now [2]
queue.push(5); //queue is now [2,5]
var i = queue.shift(); //queue is now [5], i is now 2



var stack = []
stack.push(2); //stack is now [2]
stack.push(5); //stack is now [2,5]

var i = stack.pop(); //stack is now [2], i is now 5

